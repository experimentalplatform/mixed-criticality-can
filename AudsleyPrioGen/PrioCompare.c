#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include <Windows.h>
//#define DEBUG

#define MAX_MSG_LENGTH 135

#define BIT_RATE 1000000 //bits / sec


#define F_HI 10

#define F_LO 0


#define SCHEDULABLE 1
#define UNSCHEDULABLE 0



//tx time = 135 bits * (1/500Kb/s) *1000 ms
float C  = float(float(MAX_MSG_LENGTH )/float( BIT_RATE)) * 1000 ; 

float bit_time  = float(1/float(BIT_RATE))*1000 ; //ms

float ERRMAX = float(31)* bit_time ;//ms

 double   lambda ;              // Mean rate


int NUM_OF_MSG;

int CF;

int CP;

int period_low = 10; //ms
int period_high = 1000;  //ms

//----- Function prototypes -------------------------------------------------
int    poisson(double x);       // Returns a Poisson random variable
double expon(double x);         // Returns an exponential random variable
double rand_val(int seed);      // Jain's RNG


typedef struct MsgAttr
 {
  int priority;
  bool marked;
  float txtime; //ms
  float period_low; //ms
  float period_high; //ms
  float deadline; //ms
  bool crit;
}MsgAttr;

MsgAttr MsgTable[120];
/*{
 {255 , 0 , C , 20 , 0 , 0, 0},
 {255 , 0 , C , 30 , 0 , 0, 1},
 {255 , 0 , C , 40 , 0 , 0, 1}
};*/

float Ji = 0;//0.01; //1 tick = 0.01ms
 
const float Bi = C;
 
const float Fi_low = F_LO * (ERRMAX + C);
 
const float Fi_hi = F_HI * (ERRMAX + C);

const float Ci_F = C;
 
const float Ci_Mode = float(2) * C; // Ci_Mode = C_Go_Hi + max(C_Go_Hi,C)

bool MC_Schedulability(int Prio_Under_Test, int Msg_Under_Test)
{
// static int i = 0;
 //i++;
 
 
 
 
 float Ri_s_low = 0;
 
 float Ri_s_low_prev = 0;
 
 float Ri_low = 0;
 
 float Ri_s_high = 0;
 
 float Ri_s_high_prev = 0;
 
 float Ri_high = 0;
 
 
 
 int j=0;
 
 float low_sum = 0;
 
 float high_sum_lowCrit = 0;
 
 float high_sum_highCrit = 0;
 
 #ifdef DEBUG
 printf("\n Prio Under Test %d Jitter is %f", Prio_Under_Test,Ji);
 #endif
 do{
	 Ri_s_low_prev = Ri_s_low;
	 
	 low_sum = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 { 
																//0-highest	       //64-lowest
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))
		 {	
			#ifdef DEBUG
				printf("\n priority at j %d is %d", j,MsgTable[j].priority);
			#endif	
			 low_sum += (float(ceil(float(Ri_s_low + Ji + bit_time)/float(MsgTable[j].period_low)) )*float(C)) ;
			 #ifdef DEBUG
			   printf("\n SUM = %f ",low_sum);
			 #endif
		 }
	 }
	 
	 Ri_s_low = low_sum + Bi + Fi_low ; 
	 
	 Ri_low = Ri_s_low + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s_low is %f Ri_low is %f Ri_s_low_prev is %f", Ri_s_low,Ri_low, Ri_s_low_prev);
	 #endif
	 
	 
 }while(double(Ri_s_low) != double(Ri_s_low_prev));
 
 

 if(double(Ri_low) > double(MsgTable[Msg_Under_Test].deadline))
 {
	#ifdef DEBUG
		printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
		printf("\n Ri_low %f > Deadline %f \n NOT SCHEDULABLE", Ri_low, MsgTable[Msg_Under_Test].deadline );
	#endif
 return 0;
 }
 
 else if(MsgTable[Msg_Under_Test].crit == 0)
	 return 1;
 
 else{
	  
	 do{
	 Ri_s_high_prev = Ri_s_high;
	 
	 high_sum_lowCrit = 0;
	 high_sum_highCrit = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 {
		 #ifdef DEBUG
			//printf("\n J is %d", j);
		 #endif
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))  //prio 1 > prio 2
		   if(MsgTable[j].crit == 0) //low crit && higher priority
			{
				high_sum_lowCrit += (float(ceil(float(Ri_s_low + Ji)/float(MsgTable[j].period_low)) )*float(C)) ;
				#ifdef DEBUG
					printf("\n high_sum_lowCrit = %f ",high_sum_lowCrit);
				#endif
			}
			else
			{
				high_sum_highCrit += (float(ceil(float(Ri_s_high + Ji + bit_time)/float(MsgTable[j].period_high)) )*float(C)) ;
				#ifdef DEBUG
					printf("\n high_sum_highCrit = %f ",high_sum_highCrit);
				#endif
			}
	 }
	 
	 Ri_s_high = high_sum_lowCrit + high_sum_highCrit + Bi + Fi_hi + Ci_F + Ci_Mode; 
	 
	 Ri_high = Ri_s_high + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s_high is %f Ri_high is %f Ri_s_high_prev is %f", Ri_s_high,Ri_high, Ri_s_high_prev);
	 #endif
	 
	 
        }while(double(Ri_s_high) != double(Ri_s_high_prev));
		
		
  if(double(Ri_high) > double(MsgTable[Msg_Under_Test].deadline))
  {  
  #ifdef DEBUG
	  printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
	  printf("\n Ri_high %f > Deadline \n NOT SCHEDULABLE", Ri_high, MsgTable[Msg_Under_Test].deadline);
  #endif
    return 0;
  }
  
  else
	  
	  {
		  #ifdef DEBUG
			printf("\n SCHEDULABLE");
		  #endif
          return 1;
		  
	  }
	  
	  
	  
	  
	  
	  
  }
 
 
 //printf("\n schedulability %d", i);
 //if(i%2==0)
 	//return 1;
 //else
 	//return 0;

//return 1;
}

bool initMsgSet()
{
 int i; 
 for(i=0; i< NUM_OF_MSG; i++)
  {
   if(MsgTable[i].crit == 1)
   {
    MsgTable[i].period_high = MsgTable[i].period_low / CF;
    MsgTable[i].deadline = MsgTable[i].period_high;

   }//if
   else
    {
    MsgTable[i].period_high = 0;
    MsgTable[i].deadline = MsgTable[i].period_low;

   }//else
    
  }//for

return 1;
}


bool AudsleyPrioAssign(bool (*schedulabilityTest)(int,int))
{
 int count = 0;
 int i,j;
 for(i=NUM_OF_MSG; i>=1 && count != NUM_OF_MSG; i--)
 {   
     #ifdef DEBUG
		printf("\n Prio level %d" ,i);
	 #endif
	 for (j=0; j< NUM_OF_MSG; j++)
	 {
		 if( MsgTable[j].marked != 1)
		  if(schedulabilityTest(i,j) == 1 )
			 {
				 MsgTable[j].priority = i;
				 MsgTable[j].marked = 1;
				 count++;
				 #ifdef DEBUG
					printf(" \n Msg %d assigned prio %d \n break ", j, i);
				 #endif
				 goto OUTER;
			 } 
			 
		 
	 }
	 
	  #ifdef DEBUG
		printf("\nMUST NOT REACH HERE");
	  #endif
	  return UNSCHEDULABLE;
	 
	 OUTER: ;
	 
 }
 
 return SCHEDULABLE;
}

void display()
{
	for(int i=0;i<NUM_OF_MSG;i++)
		printf("\n index %d , priority %d , marked %d, tx time %.3f , period_low %.3f, period_high %.3f,deadline %.3f, crit %d",\
					i, MsgTable[i].priority, MsgTable[i].marked,\
					MsgTable[i].txtime, MsgTable[i].period_low, MsgTable[i].period_high,\
					MsgTable[i].deadline, MsgTable[i].crit);
}



double uniform(double a, double b)
{


return rand() / (RAND_MAX + 1.0) * (b - a) + a;
}

int rand_val_seed()
{
	//srand(time(NULL));
	srand(GetTickCount());
	
	int random_val = rand();
	if(random_val == 0)
		return 1;
	else
		return random_val;

}

double rand_val(int seed)
{
  const long  a =      16807;  // Multiplier
  const long  m = 2147483647;  // Modulus
  const long  q =     127773;  // m div a
  const long  r =       2836;  // m mod a
  static long x;               // Random int value
  long        x_div_q;         // x divided by q
  long        x_mod_q;         // x modulo q
  long        x_new;           // New x value

  // Set the seed if argument is non-zero and then return zero
  if (seed > 0)
  {
    x = seed;
    return(0.0);
  }

  // RNG using integer arithmetic
  x_div_q = x / q;
  x_mod_q = x % q;
  x_new = (a * x_mod_q) - (r * x_div_q);
  if (x_new > 0)
    x = x_new;
  else
    x = x_new + m;

  // Return a random value between 0.0 and 1.0
  return((double) x / m);
}

double expon(double x)
{
  double z;                     // Uniform random number (0 < z < 1)
  double exp_value;             // Computed exponential value to be returned

  // Pull a uniform random number (0 < z < 1)
  do
  {
    z = rand_val(0);
  }
  while ((z == 0) || (z == 1));

  // Compute exponential random variable using inversion method
  exp_value = -x * log(z);

  return(exp_value);
}

int poisson(double x)
{
  int    poi_value;             // Computed Poisson value to be returned
  double t_sum;                 // Time sum value

  // Loop to generate Poisson values using exponential distribution
  poi_value = 0;
  t_sum = 0.0;
  while(1)
  {
    t_sum = t_sum + expon(x);
    if (t_sum >= 1.0) break;
    poi_value++;
  }

  if(poi_value< 5 || poi_value>period_high)
	  poi_value = poisson(x);
  
  return(poi_value);
}


void create_msg_set(int msgSetsize, int PeriodFactor, float CritFactor)
{
	
	
	float sum = 0;
	float util_low = 0;
	int j;
	
	CP = ceil(float(1)/float(CritFactor)); //criticality probability
	
	CF = PeriodFactor ; 
	
	
	
	NUM_OF_MSG = msgSetsize;
	
	Ji = float(5)*msgSetsize*float(0.01);
	
     #ifdef DEBUG
	  printf("\n Jitter = %f",Ji);
	 #endif
	
	//srand (time(NULL));
	
	
	for(j=0;j<msgSetsize;j++)
	{
		MsgTable[j].priority = 255;
		MsgTable[j].marked = 0;
		MsgTable[j].txtime = C;
		//MsgTable[j].period_low = int(floor((exp(uniform(log(period_low),log(period_high))))/period_low)*period_low);
		MsgTable[j].period_low = poisson(1.0/lambda);
		#ifdef DEBUG
			printf("\n %f",MsgTable[j].period_low);
		
		printf(" freq %f", float(1000)/MsgTable[j].period_low);
		#endif
		
		
	
	
	
	
		//MsgTable[j].period_low = int(floor(uniform(period_low,period_high)));
		
		if(j%CF == 0)
			MsgTable[j].crit = 1;
		else
			MsgTable[j].crit = 0;
		
	//	if(MsgTable[j].period_low <float(30))
		//	MsgTable[j].crit = 0;
		
	   if(MsgTable[j].crit == 1)
		{
			MsgTable[j].period_high = MsgTable[j].period_low / CF;
			MsgTable[j].deadline = MsgTable[j].period_high;

		}//if
		else
		{
			MsgTable[j].period_high = MsgTable[j].period_low;
			MsgTable[j].deadline = MsgTable[j].period_low;

		}//else
	}
	
	
}

float utilisation_lowCrit()
{
	float sum=0;
	float util_low = 0;
	for(int i=0;i<NUM_OF_MSG;i++)
	{sum+=float(float(1000)/float(MsgTable[i].period_low));
		
	}
	
	util_low = sum * float (135)/float(BIT_RATE)*float(100);
	
	if(util_low<=100)
	{
		printf("%f, ",util_low);
		return util_low;
	}
	else
		return 0;
}

void printMsgSet()
{
	FILE *fp;
	static int SetNum=0;
	
	char filename[300];
	char str[10];
	
	
	int rand_int = rand()%1000;
	
	strcpy (filename, "C:\\Users\\Sindu\\Desktop\\AudsleyPrioGen\\");
	sprintf(str,"%d",NUM_OF_MSG);
	strcat (filename, str);
	strcat(filename,"\\F_HI_");
	sprintf(str,"%d",F_HI);
	strcat (filename, str);
	strcat( filename,"\\MsgSet_");
	sprintf(str,"%d",NUM_OF_MSG);
	strcat (filename, str);
	strcat (filename, "_");
	sprintf(str,"%d",rand_int);
	strcat (filename, str);
	strcat (filename, ".h");
	
	#ifdef DEBUG
		printf("\n Filename is %s", filename);
	#endif
	
	
	//fp = fopen("MsgSet_"+NUM_OF_MSG+"_"+SetNum+".txt",w);
	
	fp = fopen(filename,"w");
	
	if(fp != NULL)
	{
		fputs("\n #include <stdio.h>\n #include \"MessageSet.h\"\n",fp);
		fputs ("\nCANMsgAttributes CANTxMsgSetArr[]={\n",fp);
		fputs("{0x21 , canMESSAGE_BOX21, 1,   0, 0xFF, 	1, 	0 },\n", fp);
		for(int j=0;j<NUM_OF_MSG;j++)
		{
			fprintf(fp,"{0x%d , canMESSAGE_BOX%d, %d ,%.0f , 0xFF, 0, 0}",MsgTable[j].priority+21,MsgTable[j].priority+21,MsgTable[j].crit,MsgTable[j].period_low);
			if(j!=NUM_OF_MSG-1)
				fprintf(fp,",\n");
		}
		fputs("\n};",fp);
	}
	
	fclose(fp);
}

bool BMC_Schedulability(int Prio_Under_Test, int Msg_Under_Test)
{
	
	
 
 float Ri_s_low = 0;
 
 float Ri_s_low_prev = 0;
 
 float Ri_low = 0;
 
 float Ri_s_high = 0;
 
 float Ri_s_high_prev = 0;
 
 float Ri_high = 0;
 
 
 
 int j=0;
 
 float low_sum = 0;
 
 float high_sum_lowCrit = 0;
 
 float high_sum_highCrit = 0;
 
 #ifdef DEBUG
 printf("\n Prio Under Test %d", Prio_Under_Test);
 #endif
 do{
	 Ri_s_low_prev = Ri_s_low;
	 
	 low_sum = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 { 
		#ifdef DEBUG
		//	printf("\n J is %d", j);
	   #endif
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))
		 {
			 low_sum += (float(ceil(float(Ri_s_low + Ji + bit_time)/float(MsgTable[j].period_low)) )*float(C)) ;
			 #ifdef DEBUG
			   printf("\n SUM = %f ",low_sum);
			 #endif
		 }
	 }
	 
	 Ri_s_low = low_sum + Bi + Fi_low ; 
	 
	 Ri_low = Ri_s_low + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s_low is %f Ri_low is %f Ri_s_low_prev is %f", Ri_s_low,Ri_low, Ri_s_low_prev);
	 #endif
	 
	 
 }while(double(Ri_s_low) != double(Ri_s_low_prev));
 
 

 if(double(Ri_low) > double(MsgTable[Msg_Under_Test].deadline))
 {
	#ifdef DEBUG
		printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
		printf("\n Ri_low %f > Deadline %f \n NOT SCHEDULABLE", Ri_low, MsgTable[Msg_Under_Test].deadline );
	#endif
 return 0;
 }
 
 else if(MsgTable[Msg_Under_Test].crit == 0)
	 return 1;
 
 else{
	  
	 do{
	 Ri_s_high_prev = Ri_s_high;
	 
	 high_sum_lowCrit = 0;
	 high_sum_highCrit = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 {
		 #ifdef DEBUG
			//printf("\n J is %d", j);
		 #endif
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))  //prio 1 > prio 2
		   if(MsgTable[j].crit == 0) //low crit && higher priority
			{
				high_sum_lowCrit += (float(ceil(float(Ri_s_high + Ji + bit_time)/float(MsgTable[j].period_low)) )*float(C)) ;
				#ifdef DEBUG
					printf("\n high_sum_lowCrit = %f ",high_sum_lowCrit);
				#endif
			}
			else
			{
				high_sum_highCrit += (float(ceil(float(Ri_s_high + Ji + bit_time)/float(MsgTable[j].period_high)) )*float(C)) ;
				#ifdef DEBUG
					printf("\n high_sum_highCrit = %f ",high_sum_highCrit);
				#endif
			}
	 }
	 
	 Ri_s_high = high_sum_lowCrit + high_sum_highCrit + Bi + Fi_hi;// + Ci_F + Ci_Mode; 
	 
	 Ri_high = Ri_s_high + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s_high is %f Ri_high is %f Ri_s_high_prev is %f", Ri_s_high,Ri_high, Ri_s_high_prev);
	 #endif
	 
	 
        }while(double(Ri_s_high) != double(Ri_s_high_prev));
		
		
  if(double(Ri_high) > double(MsgTable[Msg_Under_Test].deadline))
  {  
  #ifdef DEBUG
	  printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
	  printf("\n Ri_high %f > Deadline \n NOT SCHEDULABLE", Ri_high, MsgTable[Msg_Under_Test].deadline);
  #endif
    return 0;
  }
  
  else
	  
	  {
		  #ifdef DEBUG
			printf("\n SCHEDULABLE");
		  #endif
          return 1;
		  
	  }
	  
	
   }
}

bool BMC_AudsleyPrioAssign()
{
	for(int i=0; i<NUM_OF_MSG; i++)
		{
		
			MsgTable[i].marked = 0;
			MsgTable[i].priority=255;
		}
		
	return AudsleyPrioAssign(BMC_Schedulability);
}

int Part_CAN_DMPO_schedulability(int Prio_Under_Test, int Msg_Under_Test)
{
	 
 float period = 0;
 
 float fault = 0;
 
 float Ri_s = 0;
 
 float Ri = 0;
 
 float Ri_s_prev = 0;
 
 
 
 int j=0;
 
 float sum = 0;
 
 
 #ifdef DEBUG
 printf("\n Prio Under Test %d", Prio_Under_Test);
 #endif
 do{
	 Ri_s_prev = Ri_s;
	 
	 sum = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 { 
        if(MsgTable[j].crit == 0)
			period = MsgTable[j].period_low;
		else
			period = MsgTable[j].period_high;
		#ifdef DEBUG
		//	printf("\n J is %d", j);
	   #endif
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))
		 {
			 #ifdef DEBUG
				printf("\n priority at j %d is %d", j,MsgTable[j].priority);
			#endif	
			 sum += float(ceil(float(Ri_s + Ji + bit_time)/float(period)) )*float(C) ;
			 #ifdef DEBUG
			   printf("\n SUM = %f ",sum);
			 #endif
		 }
	 }
	 
	 Ri_s = sum + Bi + Fi_hi  ; 
	 
	 Ri = Ri_s + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s is %f Ri is %f Ri_s_prev is %f", Ri_s,Ri, Ri_s_prev);
	 #endif
	 
	 
 }while(double(Ri_s) != double(Ri_s_prev));
 
 

 if(double(Ri) > double(MsgTable[Msg_Under_Test].deadline))
 {
	#ifdef DEBUG
		printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
		printf("\n Ri %f > Deadline %f \n NOT SCHEDULABLE", Ri, MsgTable[Msg_Under_Test].deadline );
	#endif
 return 0;
 }
 
 else
	 return 1;

}

int Std_CAN_DMPO_schedulability(int Prio_Under_Test, int Msg_Under_Test)
{
	 
 float period = 0;
 
 
 float Ri_s = 0;
 
 float Ri = 0;
 
 float Ri_s_prev = 0;
 
 
 
 int j=0;
 
 float sum = 0;
 
 
 #ifdef DEBUG
 printf("\n Prio Under Test %d", Prio_Under_Test);
 #endif
 do{
	 Ri_s_prev = Ri_s;
	 
	 sum = 0;
	 
	 for(j= 0 ; j< NUM_OF_MSG ; j++)
	 { 
        period = MsgTable[j].period_high;
		#ifdef DEBUG
		//	printf("\n J is %d", j);
	   #endif
		 if(MsgTable[j].marked == 0 || (MsgTable[j].marked == 1 && MsgTable[j].priority < Prio_Under_Test  ))
		 {
			 #ifdef DEBUG
				printf("\n priority at j %d is %d", j,MsgTable[j].priority);
			#endif	
			 sum += float(ceil(float(Ri_s + Ji + bit_time)/float(period)) )*float(C) ;
			 #ifdef DEBUG
			   printf("\n SUM = %f ",sum);
			 #endif
		 }
	 }
	 
	 Ri_s = sum + Bi + Fi_hi ; 
	 
	 Ri = Ri_s + C + Ji;
	 
	 #ifdef DEBUG
	 printf("\n Ri_s is %f Ri is %f Ri_s_prev is %f", Ri_s,Ri, Ri_s_prev);
	 #endif
	 
	 
 }while(double(Ri_s) != double(Ri_s_prev));
 
 

 if(double(Ri) > double(MsgTable[Msg_Under_Test].deadline))
 {
	#ifdef DEBUG
		printf("\n message %d deadline %f",Msg_Under_Test,MsgTable[Msg_Under_Test].deadline );
		printf("\n Ri %f > Deadline %f \n NOT SCHEDULABLE", Ri, MsgTable[Msg_Under_Test].deadline );
	#endif
 return 0;
 }
 
 else
	 return 1;

}

int cmp_by_period_low(const void *a, const void *b) 
{ 
    struct MsgAttr *ia = (struct MsgAttr *)a;
    struct MsgAttr *ib = (struct MsgAttr *)b;
    return (int)(100.f*ia->period_low - 100.f*ib->period_low);
} 

int cmp_by_period_high(const void *a, const void *b) 
{ 
    struct MsgAttr *ia = (struct MsgAttr *)a;
    struct MsgAttr *ib = (struct MsgAttr *)b;
    return (int)(100.f*ia->period_high - 100.f*ib->period_high);
} 


int partCAN_DMPO()
{
	MsgAttr MsgTableLoCrit[120];
	MsgAttr MsgTableHiCrit[120];
	int i=0 ,j=0, lo=0, hi=0;
	
	for(i=0; i<NUM_OF_MSG; i++)
	{
		if(MsgTable[i].crit == 0)
			MsgTableLoCrit[lo++]=MsgTable[i];
		else
			MsgTableHiCrit[hi++]=MsgTable[i];
	}
	
	qsort(MsgTableHiCrit, hi, sizeof(struct MsgAttr), cmp_by_period_high);
	qsort(MsgTableLoCrit, lo, sizeof(struct MsgAttr), cmp_by_period_low);
	
		for(i=0; i<hi; i++)
		{
			MsgTable[i]= MsgTableHiCrit[i];
			MsgTable[i].priority=i;
			MsgTable[i].marked=1;			
		}
		
		for(j=0;j<lo;j++)
		{
			MsgTable[i]= MsgTableLoCrit[j];
			MsgTable[i].priority=i;
			MsgTable[i].marked=1;	
			i++;
		}
		
		for(i=0; i<NUM_OF_MSG; i++)
		{	
			//if(Part_CAN_DMPO_schedulability(i,MsgTable[i].priority) != 1 )
			if(MC_Schedulability(i,MsgTable[i].priority) != 1 )
				return UNSCHEDULABLE;
			else
				continue;
		}
		
		return SCHEDULABLE;	
	
	
	
}


int StdCAN_DMPO()
{
		int i=0;
		//sort by period_low-
	    qsort(MsgTable, NUM_OF_MSG, sizeof(struct MsgAttr), cmp_by_period_low);
		for(i=0; i<NUM_OF_MSG; i++)
		{
			MsgTable[i].priority = i;
			MsgTable[i].marked = 1;
		}
		
		for(i=0; i<NUM_OF_MSG; i++)
		{	
			//if(Std_CAN_DMPO_schedulability(i,MsgTable[i].priority) != 1 )
			if(MC_Schedulability(i,MsgTable[i].priority) != 1 )
				return UNSCHEDULABLE;
			else
				continue;
		}
		
		return SCHEDULABLE;	
	
}


int main(int argc,char *argv[])
{
	
 //argv[1]-msg set size
 //argv[2]-period scaling factor- must be whole number
 //argv[3]-probability of a msg being high crit - must be between 0 and 1 -  float allowed
 //argv[4]-lambda or mean value for poisson distribution - provide value according to msg set size. 
		//if too small, then util will be hiher than 100 and will get stuck in while loop
 
 

 
 int count_mixedcan = 0;
 int count_stdcan = 0;
 int count_parcan = 0;
 int count_bmcan=0;

 //for poisson distribution
 rand_val(rand_val_seed());
 lambda = atof(argv[4]);

do{ 
 create_msg_set(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
}while(!utilisation_lowCrit());//util must be lesser than 100 otherwise keep generating msg set
 
	#ifdef DISPLAY
	printf("\nBEFORE");
	display();
	#endif
 
	if(AudsleyPrioAssign(MC_Schedulability))//analysis for MixedCAN with Audsleys OPA
	{
		printMsgSet();
		count_mixedcan=1; //Set flag as 1 if successful
	}
	#ifdef DISPLAY
	printf("\nMC");
	display();
	#endif
	
	count_bmcan= BMC_AudsleyPrioAssign();//Analysis for Basic Mixed CAN with Audsley's OPA
	#ifdef DISPLAY
	printf("\nBMC");
	display();
	#endif
	/**********************************************************************/
	count_stdcan=StdCAN_DMPO();//Analysis for StdCAN with DMPO
	#ifdef DISPLAY
	printf("\nSTD");
	display();
	#endif
	
	count_parcan= partCAN_DMPO();//Analysis with Partition CAN with DMPO
	
	#ifdef DISPLAY
	printf("\nPART");
	display();
	#endif
 
 

  #ifdef DEBUG
	printf("\n F_HI %f F_LOW %f ERRMAX %f", Fi_hi, Fi_low, ERRMAX);
  #endif
	printf("%d, %d,%d, %d \n", count_mixedcan,count_stdcan,count_parcan );
 
 

 
}