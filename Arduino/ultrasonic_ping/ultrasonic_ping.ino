#include <NewPing.h>
 
#define TRIGGER_PIN 11
#define ECHO_PIN 12
#define MAX_DISTANCE 200

// NewPing setup of pins and maximum distance.
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 
 
void setup() {
   Serial.begin(9600);
}
 
void loop() {
   delay(50);
   //unsigned int uS = sonar.ping_cm();
   unsigned int echoTime = sonar.ping_median();
   unsigned int uS = sonar.convert_cm(echoTime);
   Serial.print(uS);
   Serial.println("cm");
}
