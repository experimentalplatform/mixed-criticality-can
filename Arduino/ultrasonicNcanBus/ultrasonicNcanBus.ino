/****************************************************************************
CAN-Bus -- Ultrasonic Demo
Daniel Ng
*************************************************************************/

#include <Canbus.h>
#include <NewPing.h>
 
#define TRIGGER_PIN 3
#define ECHO_PIN 4
#define MAX_DISTANCE 200
#define LED1 8
#define LED2 7
#define LEFT   A2
#define RIGHT  A5
#define UP  A1
#define DOWN  A3

// NewPing setup of pins and maximum distance.
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 
char UserInput;
int data;
char buffer[456];  //Data will be temporarily stored to this buffer before being written to the file

//********************************Setup Loop*********************************//

void setup(){
Serial.begin(9600);
pinMode(LED1, OUTPUT); 
pinMode(LED2, OUTPUT); 
pinMode(LEFT,INPUT);
pinMode(RIGHT,INPUT);
pinMode(UP,INPUT);
pinMode(DOWN,INPUT);


digitalWrite(LEFT, HIGH);
digitalWrite(RIGHT, HIGH);
digitalWrite(UP,HIGH);
digitalWrite(DOWN,HIGH);
Serial.println("CAN-Bus Demo");

if(Canbus.init(CANSPEED_500))  /* Initialise MCP2515 CAN controller at the specified speed */
  {
    Serial.println("CAN Init ok");
  } else
  {
    Serial.println("Can't init CAN");
  } 
  delay(1000);
}



//********************************Main Loop*********************************//

void loop(){


unsigned int echoTime = sonar.ping_median(5);
unsigned int uS = sonar.convert_cm(echoTime);
Serial.print(uS);
Serial.println("cm");


 int leftTrigger = -10;
 int budgetOverrun_CommonSource = -20;
 int budgetOverrun_Cascade= -30;

//if (Canbus.message_tx_ultrasonic(uS))
  //{
  //  digitalWrite(LED1, HIGH); 
  //  Serial.print(uS);
  //  delay(5); 
  //  digitalWrite(LED1, LOW);
  //  delay(5);
 // }


   
if (digitalRead(LEFT) == 0) {
    Serial.println("Left");
    if (Canbus.message_tx_laneChangeLeft(leftTrigger))
       {
     
               digitalWrite(LED2, HIGH); 

       }
 }


if (digitalRead(UP) == 0) {
       Serial.println("Budget Overrun");
       if (Canbus.message_tx_budgetOverrun_Cascade(budgetOverrun_Cascade))
          {
            digitalWrite(LED2, LOW); 
            
          }
} 


if (digitalRead(DOWN) == 0) {
       Serial.println("Budget Overrun Common Source");
       if (Canbus.message_tx_budgetOverrun_CommonSource(budgetOverrun_CommonSource))
          {
            digitalWrite(LED2, HIGH); 
            delay(2000);
            digitalWrite(LED2, LOW); 
          } 
}
}
