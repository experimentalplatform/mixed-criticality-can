
 #include <stdio.h>
 #include "MessageSet.h"

CANMsgAttributes CANTxMsgSetArr[]={
{0x21 , canMESSAGE_BOX21, 1,   0, 0xFF, 	1, 	0 },
{0x22 , canMESSAGE_BOX22, 0,   0, 0xFF, 	1, 	0 },
{0x32 , canMESSAGE_BOX32, 1 ,300 , 0xFF, 0, 0},
{0x31 , canMESSAGE_BOX31, 1 ,80 , 0xFF, 0, 0},
{0x30 , canMESSAGE_BOX30, 1 ,160 , 0xFF, 0, 0},
{0x29 , canMESSAGE_BOX29, 1 ,260 , 0xFF, 0, 0},
{0x28 , canMESSAGE_BOX28, 1 ,20 , 0xFF, 0, 0},
{0x27 , canMESSAGE_BOX27, 1 ,120 , 0xFF, 0, 0},
{0x26 , canMESSAGE_BOX26, 1 ,60 , 0xFF, 0, 0},
{0x25 , canMESSAGE_BOX25, 1 ,40 , 0xFF, 0, 0},
{0x24 , canMESSAGE_BOX24, 0 ,140 , 0xFF, 0, 0},
{0x23 , canMESSAGE_BOX23, 0 ,250 , 0xFF, 0, 0}
};