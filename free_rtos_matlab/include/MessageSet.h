#ifndef MsgSet
#define MsgSet

//#include "MsgSet_10_919.h"
#define CF 2

#define NODE1
//#define NODE2

//Define testing for carrying out the below test scenarios
//#define testing /*Uncomment any one of the macros below - Preferably uncomment MCTEST6 */

//Test scenarios- define only one at a time and comment out the rest
///*1*/#define MCTEST1 /*low crit- tx lo and hi ; transition - tx go_hi ; hi crit- tx hi only*/
///*2*/#define MCTEST2 /*if low crit msg arrives for tx earlier than guide time, do not tx it */
///*3*/#define MCTEST3 /*if high crit msg arrives for tx earlier than guide time, change sys crit, tx go_hi and tx the msg*/
///*4*/#define MCTEST4 /*Testing delay values in tx*/
///*5*/#define MCTEST5 /*Testing synchronisation with canSysCritSem*/
///*6*/#define MCTEST6 /*Testing periodic transmission*/
///*7*/#define MCTEST7 /*Testing transmission of GO_HI due to faults>F(LO)*/
//MixedCriticality in CAN variables
#define highCrit 1
#define lowCrit 0
#define TRIGGER_PRESENT 1
#define TRIGGER_ABSENT 0
#define ErrorLimitCritLow 0 //F(LO) must be ideally 0. Other values for testing only

int RxlowCritIndex=0;
int RxhighCritIndex=0;
int RxGOHiIndex=0;
int TxlowCritIndex=0;
int TxhighCritIndex=0;
signed int TxGOHiIndex=-1;
signed int TxGOLoIndex=-1;

boolean canRoutineFlag = 0; //currently unused

static signed int canMsgInitDone= -1; //status of CAN msg set initialisation -1:error others:number of hi-crit msgs
static int systemCrit = lowCrit; //Criticality level of the node
static uint32 CANGoHiTxStatus=0; //status of tx of Go_Hi msg

TickType_t CANTxMsgReleaseTime[80];
TickType_t CANTxMsgMinRespTime[12]={0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
TickType_t CANTxMsgMaxRespTime[12]={0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};
TickType_t CANTxMsgAvgRespTime[12]={0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};
TickType_t queueTime[12] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};
int count_avg[12]={0,0,0,0,0,0,0,0,0,0,0,0};

SemaphoreHandle_t canSysCritSem;

//Function prototypes
/*1*/unsigned int CANCheckErrorCount();
/*2*/unsigned int CANRxCheckifTrigger(uint32);
/*3*/signed int CANRxFindFromMsgSet(uint32);
/*4*/signed int CANRxSortCritMsg();
/*5*/void CANFlushLowCrit();
/*6*/unsigned int CANCritTransmit(canBASE_t *, uint32 , uint8 * );
/*7*/unsigned int CANTxCheckifTrigger(uint32);
/*8*/signed int CANTxFindFromMsgSet(uint32);
/*9*/signed int CANTxSortCritMsg();
/*10*/void CANFlushGOHi();
/*11*/void CANFlushAll();

typedef struct CANMsgAttributes
{
    uint32 CANMsgID; // identifier
    uint32 CANMsgBox;// message object
    uint8 CANMsgCrit; // criticality of the message
    uint16 CANMsgPeriod; // period of the message 0: aperiodic, others:period value ; in ms; 1 ms = 100 ticks
    uint8 CANMsgCritIndex; //index of msg in its criticality table
    boolean CANMsgTrigger; //Check whether the msg is GO_HI
    sint32 CANGuideTime; //Guide time for the next arrival of the msg (applicable for tx msgs only)

}CANMsgAttributes;

CANMsgAttributes CANTxMsgSetArr[];

#ifdef NODE1
CANMsgAttributes CANRxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI?*/
        {0x7F, canMESSAGE_BOX1, highCrit, 0 ,    0xFF,        1},
        {0x82, canMESSAGE_BOX2, lowCrit, 0 ,    0xFF,        0},
        {0x83, canMESSAGE_BOX3, highCrit,10,    0xFF,        0},
        {0x84, canMESSAGE_BOX4, lowCrit, 0 ,    0xFF,        0},
        {0x85, canMESSAGE_BOX5, lowCrit, 0 ,    0xFF,        0},
        {0x86, canMESSAGE_BOX6, highCrit,10,    0xFF,        0},
        {0x87, canMESSAGE_BOX7, lowCrit, 0 ,    0xFF,        0},
        {0x88, canMESSAGE_BOX8, lowCrit, 0 ,    0xFF,        0},
        {0x89, canMESSAGE_BOX9, highCrit, 10,   0xFF,        0},
        {0x8A, canMESSAGE_BOX10, highCrit, 0,    0xFF,       0},
        {0x8B, canMESSAGE_BOX11, lowCrit, 0,    0xFF,        1},
        {0x8C, canMESSAGE_BOX12, highCrit,10,   0xFF,        0}
        };

#ifdef MCTEST7
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x21,canMESSAGE_BOX21, highCrit, 0,    0xFF,       1,     0},
       { 0x61,canMESSAGE_BOX61, lowCrit, 0,    0xFF,       1,     0},
       { 0x22,canMESSAGE_BOX22, lowCrit, 1000,    0xFF,       0,     0},
       { 0x23,canMESSAGE_BOX23, highCrit, 800,    0xFF,       0,     0}
};
#endif
#endif

#ifdef NODE2
CANMsgAttributes CANRxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI?*/
        {0x21, canMESSAGE_BOX1, highCrit, 0 ,    0xFF,        1},
        {0x22, canMESSAGE_BOX2, lowCrit, 0 ,    0xFF,        0},
        {0x23, canMESSAGE_BOX3, highCrit,10,    0xFF,        0},
        {0x84, canMESSAGE_BOX4, lowCrit, 0 ,    0xFF,        0},
        {0x85, canMESSAGE_BOX5, lowCrit, 0 ,    0xFF,        0},
        {0x86, canMESSAGE_BOX6, highCrit,10,    0xFF,        0},
        {0x87, canMESSAGE_BOX7, lowCrit, 0 ,    0xFF,        0},
        {0x88, canMESSAGE_BOX8, lowCrit, 0 ,    0xFF,        0},
        {0x89, canMESSAGE_BOX9, highCrit, 10,   0xFF,        0},
        {0x8A, canMESSAGE_BOX10, highCrit, 0,    0xFF,       0},
        {0x61, canMESSAGE_BOX11, lowCrit, 0,    0xFF,        1},
        {0x8C, canMESSAGE_BOX12, highCrit,10,   0xFF,        0}
        };

#ifdef MCTEST7
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x8B,canMESSAGE_BOX21, highCrit, 0,    0xFF,       1,     0},
       { 0x7F,canMESSAGE_BOX61, lowCrit, 0,    0xFF,       1,     0},
       { 0x82,canMESSAGE_BOX22, lowCrit, 500,    0xFF,       0,     0},
       { 0x83,canMESSAGE_BOX23, highCrit, 800,    0xFF,       0,     0}
};
#endif
#endif

#ifndef testing
//    #include "MsgSet_10_0.h"
//    #include "MsgSet_15_0.h"
//    #include "MsgSet_20_0.h"
//    #include "MsgSet_25_2.h"
//    #include "MsgSet_30_4.h"
//    #include "MsgSet_35_9.h"
//    #include "MsgSet_40_9.h"
//    #include "N10CP20_394Type0.h"
//    #include "N10CP40_628Type0.h"
    #include "N10CP60_411Type0.h"
//    #include "N10CP80_913Type0.h"
//   #include "N10CP20_394Type1.h"
//    #include "N10CP40_628Type1.h"
//    #include "N10CP60_411Type1.h"
//    #include "N10CP80_913Type1.h"

//#define TIMETEST
#endif

#ifdef MCTEST1
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       1,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit,  0,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit, 0,    0xFF,       0,     0}
};
#endif


#ifdef MCTEST2
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       1,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit, 20,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit, 0,    0xFF,       0,     0}
};
#endif

#ifdef MCTEST3
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       1,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit,  0,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit,20,    0xFF,       0,     0}
};
#endif

#ifdef MCTEST4
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       0,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit, 15,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit,20,    0xFF,       0,     0}
};
#endif

#ifdef MCTEST5
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       0,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit, 15,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit,20,    0xFF,       0,     0}
};
#endif

#ifdef MCTEST6
CANMsgAttributes CANTxMsgSetArr[]={\
      /*  ID     MSGBOX         CRIT    PERIOD CRITINDEX  GO_HI   GUIDETIME */
       { 0x70,canMESSAGE_BOX17, highCrit, 0,    0xFF,       1,     0},
       { 0x71,canMESSAGE_BOX18, lowCrit, 40,    0xFF,       0,     0},
       { 0x72,canMESSAGE_BOX19, highCrit,60,    0xFF,       0,     0}
};
#endif





CANMsgAttributes CANMsgRxHighCritArr[12];
CANMsgAttributes CANMsgRxLowCritArr[12];
CANMsgAttributes CANMsgTxHighCritArr[21];
CANMsgAttributes CANMsgTxLowCritArr[21];

//Sorts Rx message set based on their criticality and stores them into CANMsgRxHighCritArr[] and CANMsgRxLowCritArr[]
signed int CANRxSortCritMsg()
{
    int i=0; //loop variable

   // int GOHiFoundFlag=0; //There could be multiple GO_HIs received from different nodes


    for(i=0; i< sizeof(CANRxMsgSetArr)/sizeof(CANRxMsgSetArr[0]); i++)
        if(CANRxMsgSetArr[i].CANMsgCrit == lowCrit)
        {
            CANMsgRxLowCritArr[RxlowCritIndex].CANMsgBox    = CANRxMsgSetArr[i].CANMsgBox;
            CANMsgRxLowCritArr[RxlowCritIndex].CANMsgCrit   = CANRxMsgSetArr[i].CANMsgCrit;
            CANMsgRxLowCritArr[RxlowCritIndex].CANMsgID     = CANRxMsgSetArr[i].CANMsgID;
            CANMsgRxLowCritArr[RxlowCritIndex].CANMsgPeriod = CANRxMsgSetArr[i].CANMsgPeriod;
            CANRxMsgSetArr[i].CANMsgCritIndex             = RxlowCritIndex;
            RxlowCritIndex++;

        }

        else if(CANRxMsgSetArr[i].CANMsgCrit == highCrit)
           {
               CANMsgRxHighCritArr[RxhighCritIndex].CANMsgBox    = CANRxMsgSetArr[i].CANMsgBox;
               CANMsgRxHighCritArr[RxhighCritIndex].CANMsgCrit   = CANRxMsgSetArr[i].CANMsgCrit;
               CANMsgRxHighCritArr[RxhighCritIndex].CANMsgID     = CANRxMsgSetArr[i].CANMsgID;
               CANMsgRxHighCritArr[RxhighCritIndex].CANMsgPeriod = CANRxMsgSetArr[i].CANMsgPeriod;
               CANRxMsgSetArr[i].CANMsgCritIndex               = RxhighCritIndex;
               RxhighCritIndex++;

           }

        else
            return -1; //message configured with invalid criticality! check CANRxMsgSetArr[]

    return RxhighCritIndex;


}


//Sorts message set based on their criticality and stores them into CANMsgHighCritArr[] and CANMsgLowCritArr[]
signed int CANTxSortCritMsg()
{
    int i=0; //loop variable

    boolean GOHiFoundFlag=0;
    boolean GOLoFoundFlag=0;


    for(i=0; i< sizeof(CANTxMsgSetArr)/sizeof(CANTxMsgSetArr[0]); i++)
    {
        CANTxMsgSetArr[i].CANGuideTime = 0;

        if(CANTxMsgSetArr[i].CANMsgCrit == lowCrit)
        {
            CANMsgTxLowCritArr[TxlowCritIndex].CANMsgBox    = CANTxMsgSetArr[i].CANMsgBox;
            CANMsgTxLowCritArr[TxlowCritIndex].CANMsgCrit   = CANTxMsgSetArr[i].CANMsgCrit;
            CANMsgTxLowCritArr[TxlowCritIndex].CANMsgID     = CANTxMsgSetArr[i].CANMsgID;
            CANMsgTxLowCritArr[TxlowCritIndex].CANMsgPeriod = CANTxMsgSetArr[i].CANMsgPeriod;
            CANTxMsgSetArr[i].CANMsgCritIndex             = TxlowCritIndex;

            if(GOLoFoundFlag == 0 && CANTxMsgSetArr[i].CANMsgTrigger == 1) //Store index of first GO_HI Tx msg
                           {
                             TxGOLoIndex = TxlowCritIndex;
                             GOLoFoundFlag = 1;
                           }
            TxlowCritIndex++;

        }

        else if(CANTxMsgSetArr[i].CANMsgCrit == highCrit)
           {
               CANMsgTxHighCritArr[TxhighCritIndex].CANMsgBox    = CANTxMsgSetArr[i].CANMsgBox;
               CANMsgTxHighCritArr[TxhighCritIndex].CANMsgCrit   = CANTxMsgSetArr[i].CANMsgCrit;
               CANMsgTxHighCritArr[TxhighCritIndex].CANMsgID     = CANRxMsgSetArr[i].CANMsgID;
               CANMsgTxHighCritArr[TxhighCritIndex].CANMsgPeriod = CANTxMsgSetArr[i].CANMsgPeriod;
               CANTxMsgSetArr[i].CANMsgCritIndex               = TxhighCritIndex;

               if(GOHiFoundFlag == 0 && CANTxMsgSetArr[i].CANMsgTrigger == 1) //Store index of first GO_HI Tx msg
                   {
                   TxGOHiIndex = TxhighCritIndex;
                   GOHiFoundFlag = 1;
                   }

               TxhighCritIndex++;

           }

        else
            return -1; //message configured with invalid criticality! check CANTxMsgSetArr[]
    }
    return TxhighCritIndex;


}

//Checks whether message belongs to the supported message set
//Return value: i. -1 - message not found in message set ,i.e. invalid message
//              ii. 0 to m - index where message was found in message set , i.e. valid message
signed int CANRxFindFromMsgSet(uint32 mailBox)
{
    // flag to denote whether message was found in the message set
    //-1                                          :   Message Not found
    //0 to (Number of messages in message set -1) :   Message found at the index.
    signed int CANRxMsgFoundFlag = -1;

    //Loop iteration variable
    int i=0;

    //Look for matching message object in CANRxMsgSetArr[]
    for(i=0; i< sizeof(CANRxMsgSetArr)/sizeof(CANRxMsgSetArr[0]); i++)
        {
            if(mailBox == CANRxMsgSetArr[i].CANMsgBox) //if message is found, then
            {
                //Set message found flag as the index of the message in CANRxMsgSetArr[]
                    CANRxMsgFoundFlag = i;
               //break out of for loop
                    break;


            }

            else
            {   //Message not found in message set yet. Search at next index
                continue;
            }
        }

    return CANRxMsgFoundFlag;

}



//Returns Current Error Count of CAN Node canREG1
unsigned int CANCheckErrorCount()
{

    uint8 CANErrorLevel = 0;

    //CANErrorLevel = canGetErrorLevel(canREG1);
    uint8 TXerrCount=0 ;
    uint8 RXerrCount=0 ;

    //Access error count registers
    TXerrCount = (uint8)(canREG1->EERC & 0xFF) ;
    RXerrCount = (uint8)((canREG1->EERC & 0x7F00)>>8);

    //Sum of error counter
    CANErrorLevel = TXerrCount + RXerrCount;
    // CANErrorLevel = (uint8)(canREG1->EERC & 0xFF) + (uint8)((canREG1->EERC & 0x7F00)>>8);
    //EERC- bit 14 to bit 8 - REC ; bit 7 to 0 - TEC

    //CANErrorLevel = (TxerrCount > RxerrCount)? TxerrCount : RxerrCount;

    return CANErrorLevel;
}

unsigned int CANRxCheckifTrigger(uint32 mailBox)
{
    sint32 msgIndex;
    //int msgValid = 0;


    msgIndex=CANRxFindFromMsgSet(mailBox); //check if message is configured in CANRxMsgSetArr[]
    /*Alternative implementation - check only if CANMsgHiCritArr[] ! */

    //check if msg ID is valid
    if(msgIndex!=-1) //if msg is valid
        if(CANRxMsgSetArr[msgIndex].CANMsgCritIndex!=0xFF) //is msg sorting done
            if( systemCrit == lowCrit )
                {
                if(CANRxMsgSetArr[msgIndex].CANMsgCrit == highCrit && CANRxMsgSetArr[msgIndex].CANMsgTrigger==1  )//if the msg is high crit and is a go_hi msg

                    return TRIGGER_PRESENT; //the msg is a trigger msg
                }
            else
            {
                if(CANRxMsgSetArr[msgIndex].CANMsgCrit == lowCrit && CANRxMsgSetArr[msgIndex].CANMsgTrigger==1  )//if the msg is high crit and is a go_hi msg

                      return TRIGGER_PRESENT; //the msg is a trigger msg
             }
 return TRIGGER_ABSENT;
}


void CANFlushGOHi()
{

    uint32 txGoHiMsgObj;
    uint32 txGoHiMsgTxPending;
      if(canMsgInitDone!= -1) //if msg set has been initialised already
       {

               //Extract msg obj of go_hi msg
               txGoHiMsgObj = CANMsgTxHighCritArr[TxGOHiIndex].CANMsgBox;
               //Get tx request flag for the msg object
               txGoHiMsgTxPending = canIsTxMessagePending(canREG1, txGoHiMsgObj);

               if(txGoHiMsgTxPending == 1) //if transmission is pending
               {
                   /** - Wait until IF1 is ready for use */
                   while ((canREG1->IF1STAT & 0x80U) ==0x80U)
                      {
                      } /* Wait */


                   //Reset Tx Request flag to 0
                   canREG1->IF1CMD = 0x93;
                   //bit 0 = 1-write
                   //bit 1 = 0-mask
                   //bit 2 = 0- arb
                   //bit 3 = 1- control
                   //bit 4 = 0- clrintrpend
                   //bit 5 = 0- txrqst
                   //bit 6 = 1-data A
                   //bit 7 = 1- dataB

                   //Copy message box to IF1
                   canREG1->IF1NO = (uint8)txGoHiMsgObj;

               }


           }




}

void CANFlushAll()
{
    int i=0; //loop variable
     uint32 MsgObj;
     uint32 MsgTxPending;
    if(canMsgInitDone!= -1) //if msg set has been initialised already
     {
        for(i=0; i< sizeof(CANTxMsgSetArr)/sizeof(CANTxMsgSetArr[0]); i++)


         {
             //Extract msg obj of low crit msg
             MsgObj = CANTxMsgSetArr[i].CANMsgBox;
             //Get tx request flag for the msg object
             MsgTxPending = canIsTxMessagePending(canREG1, MsgObj);

             if(MsgTxPending == 1) //if transmission is pending
             {
                 /** - Wait until IF1 is ready for use */
                 while ((canREG1->IF1STAT & 0x80U) ==0x80U)
                    {
                    } /* Wait */




                 //Reset Tx Request flag to 0
                 canREG1->IF1CMD = 0x93;
                 //bit 0 = 1-write
                 //bit 1 = 0-mask
                 //bit 2 = 0- arb
                 //bit 3 = 1- control
                 //bit 4 = 0- clrintrpend
                 //bit 5 = 0- txrqst
                 //bit 6 = 1-data A
                 //bit 7 = 1- dataB

                 //Copy message box to IF1
                 canREG1->IF1NO = (uint8)MsgObj;

             }


         }
     }




}


void CANFlushLowCrit()
{
    int i=0; //loop variable
    uint32 lowCritMsgObj;
    uint32 lowCritMsgTxPending;
   if(canMsgInitDone!= -1) //if msg set has been initialised already
    {
       for(i=0; i<TxlowCritIndex; i++)


        {
            //Extract msg obj of low crit msg
            lowCritMsgObj = CANMsgTxLowCritArr[i].CANMsgBox;
            //Get tx request flag for the msg object
            lowCritMsgTxPending = canIsTxMessagePending(canREG1, lowCritMsgObj);

            if(lowCritMsgTxPending != 0) //if transmission is pending
            {
                /** - Wait until IF1 is ready for use */
                while ((canREG1->IF1STAT & 0x80U) ==0x80U)
                   {
                   } /* Wait */




                //Reset Tx Request flag to 0
                canREG1->IF1CMD = 0x93;
                //bit 0 = 1-write
                //bit 1 = 0-mask
                //bit 2 = 0- arb
                //bit 3 = 1- control
                //bit 4 = 0- clrintrpend
                //bit 5 = 0- txrqst
                //bit 6 = 1-data A
                //bit 7 = 1- dataB

                //Copy message box to IF1
                canREG1->IF1NO = (uint8)lowCritMsgObj;


            }


        }
    }



}


unsigned int CANTxCheckifTrigger(uint32 mailBox)
{
 int returnval = 0;

 if(systemCrit == lowCrit)
 {
     if(CANMsgTxHighCritArr[TxGOHiIndex].CANMsgBox == mailBox)
         returnval = 1;
 }
 else
 {
     if(CANMsgTxLowCritArr[TxGOLoIndex].CANMsgBox == mailBox)
              returnval = 1;
 }


 return returnval;
}


//Return value: i. -1 - message not found in message set ,i.e. invalid message
//              ii. 0 to m - index where message was found in message set , i.e. valid message
signed int CANTxFindFromMsgSet(uint32 mailBox)
{
    // flag to denote whether message was found in the message set
    //-1                                          :   Message Not found
    //0 to (Number of messages in message set -1) :   Message found at the index.
    signed int CANTxMsgFoundFlag = -1;

    //Loop iteration variable
    int i=0;

    //Look for matching message object in CANRxMsgSetArr[]
    for(i=0; i< sizeof(CANTxMsgSetArr)/sizeof(CANTxMsgSetArr[0]); i++)
        {
            if(mailBox == CANTxMsgSetArr[i].CANMsgBox) //if message is found, then
            {
                //Set message found flag as the index of the message in CANRxMsgSetArr[]
                    CANTxMsgFoundFlag = i;
               //break out of for loop
                    break;


            }

            else
            {   //Message not found in message set yet. Search at next index
                continue;
            }
        }

    return CANTxMsgFoundFlag;

}


//Performs the output routine
//return values:
//0 - msg not found in msg set, tx did not occur
//1 - low crit msg arrived too early, tx did not occur
//2- tx successful

unsigned int CANCritTransmit(canBASE_t *node, uint32 messageBox, uint8 * data)
{

 /*   BEGIN PSEUDO CODE
    output(M) is -- called by application code
    t := clock
    if not Valid(M) then return <invalid> end if

    if Crit_Level = LO then
        if Trigger(M) then
            send(M)
            Crit_Level := HI
            flushALL
        else if t < G[M] then -- too early for LO mode
            if Crit(M) = HI then
                send(Go_HI)
                send(M)
                Crit_Level := HI
                flushALL
            else
                return <invalid, too early>
            end if
        else
            G[M] := max(G[M], t - J[M]) + T[M]
            send(M)
        end if

    else -- in HI mode
        if Crit(M) = HI then send(M) end if
    end if

    return <success>

    END PSEUDOCODE

    */

    int returnval = 0;

    int CANTxStatus = 0;
    static int count;

    int msgIndex = -1;

    static uint8 errorcount[8] = {0,0,0,0,0,0,0,0};

    sint32 currTime;
//    TickType_t queueTime = 0;
    sint32 queueJitter = sizeof(CANTxMsgSetArr)/sizeof(CANTxMsgSetArr[0]) * 4;
    sint32 diff = 0;
   // sint32 findtime=0;
    static sint32 ExecutionTime = 0;
    sint32 EndTime = 0;
    TickType_t RespTime =0;

    currTime = xTaskGetTickCount();

    msgIndex = CANTxFindFromMsgSet(messageBox);

    errorcount[0] = CANCheckErrorCount();

    if(msgIndex == -1)
       {
        returnval = 0;
        return returnval;
       }
    //findtime= xTaskGetTickCount();

    if(xSemaphoreTake(canSysCritSem, 0) == pdTRUE)
        {

        //Check error counter values while the node is in LO-crit mode
           if(systemCrit == lowCrit && (errorcount[0] = CANCheckErrorCount()) > ErrorLimitCritLow )
              {
                 //Change system Criticality from Low to High
                  systemCrit = highCrit;
                 //flush all low crit
                  CANFlushLowCrit();
                 // send go_hi
                   errorcount[1]=1;
                  canTransmit(canREG1,canMESSAGE_BOX21 , errorcount);
              }

        if(systemCrit == lowCrit) //Check if system is currently in Low crit mode
        {

            if(CANTxCheckifTrigger(messageBox) == 1) //Check if the msg to be transmitted is the GO_HI msg
            {

                //Transmit the msg
                CANTxStatus = canTransmit(canREG1, messageBox, data );
                //queue time
                queueTime[msgIndex] = xTaskGetTickCount();
                //Change system Criticality from Low to High
                systemCrit = highCrit;
                //flush all low crit
                CANFlushLowCrit();
                returnval = 2; //Msg was transmitted successfully
            }

            else if(currTime < CANTxMsgSetArr[msgIndex].CANGuideTime ) //Msg arrived too early in Lo Crit mode
            {
                if(CANTxMsgSetArr[msgIndex].CANMsgCrit == highCrit) //if a high crit msg arrived too early in low crit mode
                {
                    //Send GO_HI msg
                    CANTxStatus = canTransmit(canREG1,canMESSAGE_BOX21 ,data);

                    //Send original message
                    CANTxStatus = canTransmit(canREG1, messageBox, data );
                    //Get queue time
                    queueTime[msgIndex] = xTaskGetTickCount();
                    //Change system Criticality from Low to High
                    systemCrit = highCrit;
                    //flush all low crit
                    CANFlushLowCrit();
                    returnval = 2; //Msg was transmitted successfully
                }

                else
                    returnval = 1; //low crit msg arrived too early
            }
            else
            {

                //Transmit msg
                CANTxStatus = canTransmit(canREG1, messageBox, data );
                //Get queue time
                 queueTime[msgIndex] = xTaskGetTickCount();
                returnval = 2;
            }

            //Calculate next guide time

             diff = currTime - queueJitter ;
             CANTxMsgSetArr[msgIndex].CANGuideTime =  (((signed)(CANTxMsgSetArr[msgIndex].CANGuideTime > (signed)(diff)))?\
                                                        CANTxMsgSetArr[msgIndex].CANGuideTime :\
                                                        (currTime - queueJitter))\
                                                        +(CANTxMsgSetArr[msgIndex].CANMsgPeriod*100); //100 ticks= 1ms



        }

        else
        { //System is in High Crit mode
            if(CANTxMsgSetArr[msgIndex].CANMsgCrit == highCrit)
            {
                //Transmit msg
                CANTxStatus = canTransmit(canREG1, messageBox, data );
                //queue time
                queueTime[msgIndex] = xTaskGetTickCount();
                returnval = 2;
            }
            else
            {

                if(CANTxCheckifTrigger(messageBox) == 1) //Check if the msg to be transmitted is the GO_LOW msg
                   {
                              //Change system Criticality from High to Low
                              systemCrit = lowCrit;
                              //Transmit the msg
                              CANTxStatus = canTransmit(canREG1, messageBox, data );
                              //queue time
                              queueTime[msgIndex] = xTaskGetTickCount();
                              returnval = 2; //Msg was transmitted successfully
                    }

                else
                    returnval = 1; //low crit msg queued for Tx in Hi Crit mode
                              // low crit msgs are not allowed for tx in high Crit mode

            }
         }
        xSemaphoreGive(canSysCritSem);

        EndTime = xTaskGetTickCount();
        ExecutionTime = EndTime - currTime ;

    }

    else
        returnval = 0;
#ifdef TIMETEST
    if(returnval == 2)
    {
        if( count < 10000){
    //    while(canIsTxMessagePending(canREG1, CANTxMsgSetArr[msgIndex].CANMsgBox)!=0); //FOR QUEUEING DELAY
   //     RespTime = xTaskGetTickCount() - CANTxMsgReleaseTime[msgIndex]; //FOR QUEUEING DELAY
         if(CANTxMsgSetArr[msgIndex].CANMsgBox == canMESSAGE_BOX21 || CANTxMsgSetArr[msgIndex].CANMsgBox == canMESSAGE_BOX22)
         {
             count = 0;
             for(count = 0; count <12 ; count ++)
                          CANTxMsgAvgRespTime[count]=CANTxMsgAvgRespTime[count]/count_avg[count];

             for(count = 0; count <12 ; count ++)
             {

             CANTxMsgMinRespTime[count]=0xFFFFFFFF;
             CANTxMsgMaxRespTime[count]=0x0;
             CANTxMsgAvgRespTime[count]=0x0;
             count_avg[count]=0;
             }
             count = 0;

         }
         RespTime = queueTime - CANTxMsgReleaseTime[msgIndex];
        if(RespTime > CANTxMsgMaxRespTime[msgIndex])
            CANTxMsgMaxRespTime[msgIndex]=RespTime;
        else
            {
            if(RespTime < CANTxMsgMinRespTime[msgIndex])
                CANTxMsgMinRespTime[msgIndex] = RespTime;
            }
        count++;
        count_avg[msgIndex]++;
        //CANTxMsgAvgRespTime[msgIndex]=CANTxMsgAvgRespTime[msgIndex]+((RespTime-CANTxMsgAvgRespTime[msgIndex])/count_avg[msgIndex]);
        CANTxMsgAvgRespTime[msgIndex]+=RespTime;
        //count_avg[msgIndex]++;

        }
       /* else
        {
           // int j=0;
            //for(j=0;j<12;j++)
              //  CANTxMsgAvgRespTime[j]=CANTxMsgAvgRespTime[j]/count_avg[j];
            count+=1;
        }*/
    }
#endif



   // }
    return returnval;
}

boolean CANGetDir(canBASE_t *node, uint32 messageBox)
{
    boolean   msgBoxDir  = 0U;


    /** - Wait until IF2 is ready for use */
    while ((node->IF2STAT & 0x80U) ==0x80U)
    {
    } /* Wait */

    /** - Configure IF2 for
    *     - Message direction - Read
    *     - Data Read
    *     - Clears NewDat bit in the message object.
    */
    node->IF2CMD = 0x20U;

    /** - Copy message box number into IF2 */
    /*SAFETYMCUSW 93 S MR: 6.1,6.2,10.1,10.2,10.3,10.4 <APPROVED> "LDRA Tool issue" */
    node->IF2NO = (uint8) messageBox;

    /** - Wait until data are copied into IF2 */
    while ((node->IF2STAT & 0x80U) ==0x80U)
    {
    } /* Wait */

    /* Read Message Box ID from Arbitration register. */
    msgBoxDir = (boolean)((node->IF2ARB & 0x20000000U)>>29)&(0x1u);

    return msgBoxDir;

}

void CANTxGetQueueDelay(uint32 messageBox)
{
    static uint16 count = 0;

    TickType_t currTime = xTaskGetTickCount();
    TickType_t RespTime = 0;
    int msgIndex = CANTxFindFromMsgSet(messageBox);

    if(CANTxMsgSetArr[msgIndex].CANMsgBox == canMESSAGE_BOX21 || CANTxMsgSetArr[msgIndex].CANMsgBox == canMESSAGE_BOX22)
            {
                count = 0;
                for(count = 0; count <12 ; count ++)
                             CANTxMsgAvgRespTime[count]=CANTxMsgAvgRespTime[count]/count_avg[count];

                for(count = 0; count <12 ; count ++)
                {

                    CANTxMsgMinRespTime[count]=0xFFFFFFFF;
                    CANTxMsgMaxRespTime[count]=0x0;
                    CANTxMsgAvgRespTime[count]=0x0;
                    count_avg[count]=0;
                }
                count = 0;

            }

    RespTime = currTime - queueTime[msgIndex];
    if(RespTime > CANTxMsgMaxRespTime[msgIndex])
                CANTxMsgMaxRespTime[msgIndex]=RespTime;
            else
                {
                if(RespTime < CANTxMsgMinRespTime[msgIndex])
                    CANTxMsgMinRespTime[msgIndex] = RespTime;
                }
            count++;
            count_avg[msgIndex]++;
            //CANTxMsgAvgRespTime[msgIndex]=CANTxMsgAvgRespTime[msgIndex]+((RespTime-CANTxMsgAvgRespTime[msgIndex])/count_avg[msgIndex]);
            CANTxMsgAvgRespTime[msgIndex]+=RespTime;



}


#endif
